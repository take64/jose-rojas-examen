<?PHP
	require("connexionbd.php");
	$result1= mysqli_query ($link, "SELECT * FROM proveedor");
	$result2a= mysqli_query ($link, "SELECT * FROM producto");
	$result2b= mysqli_query ($link, "SELECT * FROM producto");
	$result2c= mysqli_query ($link, "SELECT * FROM producto");
	$result2d= mysqli_query ($link, "SELECT * FROM producto");
	$result3= mysqli_query ($link, "SELECT MAX(numero) AS numero FROM orden_compra");
	$numero = 1;
	$arreglo_productos = array();
	while($row = mysqli_fetch_array($result3)) {
		$numero = $row['numero'] + 1;
	}
	while($row = mysqli_fetch_array($result2d, MYSQLI_ASSOC)) {
		array_push($arreglo_productos, $row);
	}

?>
<!DOCTYPE html>
<html>

<head>
		<link rel="stylesheet" type = "text/css" href="bootstrap-3.3.5-dist/css/bootstrap.min.css">
		<link rel="stylesheet" type = "text/css" href="estilo.css">
		<script src="bootstrap-3.3.5-dist/js/jquery-2.1.4.min.js"></script>
		<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
	<script>
		var arreglo_productos = <?php echo json_encode($arreglo_productos) ?>;
		console.log(arreglo_productos);

		function cambio_selectbox1(codigoProducto){
			var monto = 0;
			var cantidadInput = document.getElementById("cantidad1"); // obtenemos el input cantidad de productos
			for(var i = 0; i < arreglo_productos.length; i++) {  // recorriendo el arreglo de productos
				if(arreglo_productos[i].codigo == codigoProducto){
					monto = arreglo_productos[i].precio * cantidadInput.value; // Calculamos el monto
				}
			}
			var precioInput = document.getElementById("precio1"); //obtenemos el input del precio
			precioInput.value = monto; //agregamos el precio
		}
		function cambio_selectbox2(codigoProducto){
			var monto = 0;
			var cantidadInput = document.getElementById("cantidad2"); // obtenemos el input cantidad de productos
			for(var i = 0; i < arreglo_productos.length; i++) {  // recorriendo el arreglo de productos
				if(arreglo_productos[i].codigo == codigoProducto){
					monto = arreglo_productos[i].precio * cantidadInput.value; // Calculamos el monto
				}
			}
			var precioInput = document.getElementById("precio2"); //obtenemos el input del precio
			precioInput.value = monto; //agregamos el precio
		}
		function cambio_selectbox3(codigoProducto){
			var monto = 0;
			var cantidadInput = document.getElementById("cantidad3"); // obtenemos el input cantidad de productos
			for(var i = 0; i < arreglo_productos.length; i++) {  // recorriendo el arreglo de productos
				if(arreglo_productos[i].codigo == codigoProducto){
					monto = arreglo_productos[i].precio * cantidadInput.value; // Calculamos el monto
				}
			}
			var precioInput = document.getElementById("precio3"); //obtenemos el input del precio
			precioInput.value = monto; //agregamos el precio
		}
		function cambio_cantidad1(cantidad) {
			var monto = 0;
			var productoSelect = document.getElementById("producto1");
			for(var i = 0; i < arreglo_productos.length; i++) {  // recorriendo el arreglo de productos
				if(arreglo_productos[i].codigo == productoSelect.value){
					monto = arreglo_productos[i].precio * cantidad; // Calculamos el monto
				}
			}
			var precioInput = document.getElementById("precio1"); //obtenemos el input del precio
			precioInput.value = monto; //agregamos el precio
		}
		function cambio_cantidad2(cantidad) {
			var monto = 0;
			var productoSelect = document.getElementById("producto2");
			for(var i = 0; i < arreglo_productos.length; i++) {  // recorriendo el arreglo de productos
				if(arreglo_productos[i].codigo == productoSelect.value){
					monto = arreglo_productos[i].precio * cantidad; // Calculamos el monto
				}
			}
			var precioInput = document.getElementById("precio2"); //obtenemos el input del precio
			precioInput.value = monto; //agregamos el precio
		}
		function cambio_cantidad3(cantidad) {
			var monto = 0;
			var productoSelect = document.getElementById("producto3");
			for(var i = 0; i < arreglo_productos.length; i++) {  // recorriendo el arreglo de productos
				if(arreglo_productos[i].codigo == productoSelect.value){
					monto = arreglo_productos[i].precio * cantidad; // Calculamos el monto
				}
			}
			var precioInput = document.getElementById("precio3"); //obtenemos el input del precio
			precioInput.value = monto; //agregamos el precio
		}
	</script>
</head>

<body>

	<form action="crearoc.php" method="post">
		<fieldset>
			<legend>Orden de compra</legend>
			<table>
				<tr>
					<td>Numero: </td>
					<td><input type="text" value="<?php echo $numero;?>" disabled /></td>
					<td>Fecha: </td>
					<td><input type="date" name="fecha" /></td>
				</tr>
				<tr>
					<td>Proveedor: </td>
					<td>
						<select name="proveedor">
							<?php while($row = mysqli_fetch_array($result1)) {
								printf("<option value='%s'>%s</option>", $row['idn'], $row['nombre']);
							}
							?>
						</select>
					</td>
					<td></td>
					<td></td>
				</tr>
			</table><br /><br />
			<fieldset>
				<legend>Productos</legend>
				<table>
					<tr>
						<td>Producto 1:</td>
						<td>
							<select name="producto1" id="producto1" onchange="cambio_selectbox1(this.value);">
								<?php while($row = mysqli_fetch_array($result2a)) {
									printf("<option value='%s'>%s</option>", $row['codigo'], $row['nombre']);
								}
								?>
							</select>
						</td>
						<td>Cantidad:</td>
						<td><input type="number" name="cantidad1" id="cantidad1" value="0" onchange="cambio_cantidad1(this.value);" /></td>
						<td>Precio:</td>
						<td><input type="text" name="precio1" id="precio1" value="0" /></td>
					</tr>
					<tr>
						<td>Producto 2:</td>
						<td>
							<select name="producto2" id="producto2" onchange="cambio_selectbox2(this.value);">
								<?php while($row = mysqli_fetch_array($result2b)) {
									printf("<option value='%s'>%s</option>", $row['codigo'], $row['nombre']);
								}
								?>
							</select>
						</td>
						<td>Cantidad:</td>
						<td><input type="number" name="cantidad2" id="cantidad2" value="0" onchange="cambio_cantidad2(this.value);" /></td>
						<td>Precio:</td>
						<td><input type="text" name="precio2" id="precio2" value="0" /></td>
					</tr>
					<tr>
						<td>Producto 3:</td>
						<td>
							<select name="producto3" id="producto3" onchange="cambio_selectbox3(this.value);">
								<?php while($row = mysqli_fetch_array($result2c)) {
									printf("<option value='%s'>%s</option>", $row['codigo'], $row['nombre']);
								}
								?>
							</select>
						</td>
						<td>Cantidad:</td>
						<td><input type="number" name="cantidad3" id="cantidad3" value="0"  onchange="cambio_cantidad3(this.value);"/></td>
						<td>Precio:</td>
						<td><input type="text" name="precio3" id="precio3" value="0" /></td>
					</tr>
				</table>
			</fieldset><br /><br />
			<input type="submit" value="Crear Orden" />
		</fieldset>
	</form>
</body>

</html>
